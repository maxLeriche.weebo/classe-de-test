﻿using System;
using System.Collections.Generic;
using System.Text;

namespace alice
{
    class Navire
    {
        private string noLloyd;
        private string nomNavire;
        private string libelléFret;
        private int qtéFret = 0;

        Navire(string unNum,string unNom)
        {
            this.noLloyd = unNum;
            this.nomNavire = unNom;
        }

        public int getQtéFret() { return this.qtéFret; }
        public Boolean estDéchargé() { if (this.qtéFret == 0) { return true; } return false; }
        public Boolean décharger(int qtéAdécharger)
        {
            if (this.qtéFret>=qtéAdécharger)
            {
                this.qtéFret -= qtéAdécharger;
                return true;
            }
            else
            {
                return false;
            }
        }
        public void setFret(string unLibelle,int uneQte)
        {
            this.libelléFret = unLibelle;
            this.qtéFret = uneQte;
        }

    }
}
