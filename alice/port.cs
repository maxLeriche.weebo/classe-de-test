﻿using System;
using System.Collections.Generic;
using System.Text;

namespace alice
{
    class PORT
    {
        private List<STOCKAGE> lesStockage= new List<STOCKAGE>();

        public Boolean déchargement(Navire unNavire)
        {
            bool plein;
            if (unNavire.getQtéFret()>0)
            {
                plein = true;
                foreach (STOCKAGE swap in lesStockage)
                {
                    if (plein) //on test Deux fois la quantité de fret dans le navire une fois avant la boucle pour gagner la du temps si le navire est vide et une autre fois pour éviter de vider un navire vider précédement
                    {
                        if (swap.getCapaDispo() > 0)
                        {
                            if(swap.getCapaDispo() > unNavire.getQtéFret())
                            {
                                swap.stocker(unNavire.getQtéFret());
                                unNavire.décharger(unNavire.getQtéFret());
                                plein = false;
                            }
                            else if(unNavire.getQtéFret()> swap.getCapaDispo())
                            {
                                unNavire.décharger(unNavire.getQtéFret() - swap.getCapaDispo());
                                swap.stocker(swap.getCapaDispo());
                            }
                            else
                            {
                                swap.stocker(unNavire.getQtéFret());
                                unNavire.décharger(unNavire.getQtéFret());
                                plein = false;
                            }
                        }
                    }
                    
                }
            }
            else
            {
                return false;
            }
            return true;
        }
    }
}
