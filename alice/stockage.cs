﻿using System;
using System.Collections.Generic;
using System.Text;

namespace alice
{
    class STOCKAGE
    {
        private int capaDispo;
        private int capaMAX;
        STOCKAGE(int newcapamax)
        {
            this.capaDispo = newcapamax;
            this.capaMAX = newcapamax;
        }
        public int getCapaDispo() { return this.capaDispo; }
        public int getCapaMax() { return this.capaMAX; }

        public Boolean stocker(int qtéAstocker)
        {
            if(qtéAstocker>capaDispo)
            {
                return false;
            }
            else
            {
                this.capaDispo -= qtéAstocker;
                return true;
            }
        }

        public Boolean estVide()
        {
            if(capaDispo==capaMAX)
            {
                return true;
            }
            return false;
        }

        public Boolean estRemplie()
        {
            if (capaDispo<=0)
            {
                return true;
            }
            return false;
        }
    }
}
